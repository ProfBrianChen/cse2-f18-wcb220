// Lab 04, William Barish

public class CardGenerator {
  public static void main (String [] args){
    
    int userCard;
    String cardSuit = " ";
    userCard = (int) (Math.random() * 52 + 1);
    System.out.println(userCard);
    if ((userCard >= 1) && (userCard <= 13)){
      cardSuit = "diamonds";
    }
    else if ((userCard >= 14) && (userCard <= 26)){
      cardSuit = "clubs";
    }
    else if ((userCard >= 27) && (userCard <=39)){
      cardSuit = "hearts";
    }
    else if ((userCard >= 40) && (userCard <= 52)){
      cardSuit = "spades";
    }
    
    
    switch (userCard){
      case 1:
        System.out.println("Ace of " + cardSuit);
        break;
      case 2:
        System.out.println("2 of " + cardSuit);
        break;
      case 3:
        System.out.println("3 of " + cardSuit);
        break;
      case 4:
        System.out.println("4 of " + cardSuit);
        break;
      case 5:
        System.out.println("5 of " + cardSuit);
        break;
      case 6:
        System.out.println("6 of " + cardSuit);
        break;
      case 7:
        System.out.println("7 of " + cardSuit);
        break;
      case 8:
        System.out.println("8 of " + cardSuit);
        break;
      case 9:
        System.out.println("9 of " + cardSuit);
        break;
      case 10:
        System.out.println("10 of " + cardSuit);
        break;
      case 11:
        System.out.println("Jack of " + cardSuit);
        break;
      case 12:
        System.out.println("Queen of " + cardSuit);
        break;
      case 13:
        System.out.println("King of " + cardSuit);
        break;
      case 14:
        System.out.println("Ace of " + cardSuit);
        break;
      case 15:
        System.out.println("2 of " + cardSuit);
        break;
      case 16:
        System.out.println("3 of " + cardSuit);
        break;
      case 17:
        System.out.println("4 of " + cardSuit);
        break;
      case 18:
        System.out.println("5 of " + cardSuit);
        break;
      case 19:
        System.out.println("6 of " + cardSuit);
        break;
      case 20:
        System.out.println("7 of " + cardSuit);
        break;
      case 21:
        System.out.println("8 of " + cardSuit);
        break;
      case 22:
        System.out.println("9 of " + cardSuit);
        break;
      case 23:
        System.out.println("10 of " + cardSuit);
        break;
      case 24:
        System.out.println("Jack of " + cardSuit);
        break;
      case 25:
        System.out.println("Queen of " + cardSuit);
        break;
      case 26:
        System.out.println("King of " + cardSuit);
        break;
      case 27:
        System.out.println("Ace of " + cardSuit);
        break;
      case 28:
        System.out.println("2 of " + cardSuit);
        break;
      case 29:
        System.out.println("3 of " + cardSuit);
        break;
      case 30:
        System.out.println("4 of " + cardSuit);
        break;
      case 31:
        System.out.println("5 of " + cardSuit);
        break;
      case 32:
        System.out.println("6 of " + cardSuit);
        break;
      case 33:
        System.out.println("7 of " + cardSuit);
        break;
      case 34:
        System.out.println("8 of " + cardSuit);
        break;
      case 35:
        System.out.println("9 of " + cardSuit);
        break;
      case 36:
        System.out.println("10 of " + cardSuit);
        break;
      case 37:
        System.out.println("Jack of " + cardSuit);
        break;
      case 38:
        System.out.println("Queen of " + cardSuit);
        break;
      case 39:
        System.out.println("King of " + cardSuit);
        break;
      case 40:
        System.out.println("Ace of " + cardSuit);
        break;
      case 41:
        System.out.println("2 of " + cardSuit);
        break;
      case 42:
        System.out.println("3 of " + cardSuit);
        break;
      case 43:
        System.out.println("4 of " + cardSuit);
        break;
      case 44:
        System.out.println("5 of " + cardSuit);
        break;
      case 45:
        System.out.println("6 of " + cardSuit);
        break;
      case 46:
        System.out.println("7 of " + cardSuit);
        break;
      case 47:
        System.out.println("8 of " + cardSuit);
        break;
      case 48:
        System.out.println("9 of " + cardSuit);
        break;
      case 49:
        System.out.println("10 of " + cardSuit);
        break;
      case 50:
        System.out.println("Jack of " + cardSuit);
        break;
      case 51:
        System.out.println("Queen of " + cardSuit);
        break;
      case 52:
        System.out.println("King of " + cardSuit);
        break;
    }
      
    }
  }
