//hw03, Convert.java, William Barish 9/18/2018
import java.util.Scanner;

public class Convert{
  public static void main (String [] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    double areaCubicMiles;
     
    System.out.print("Enter the affected area in acres: ");
    double areaAcres = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in the affected area: ");
    double areaRainfall = myScanner.nextDouble();
    
    areaCubicMiles = (((areaAcres*areaRainfall)*27154.2857)*9.08169e-13);
      System.out.println(areaCubicMiles+" cubic miles");
    
  }
}