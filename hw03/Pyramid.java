//hw03, Pyramid.java, William Barish 9/18/2018
import java.util.Scanner;

public class Pyramid{
  public static void main (String [] args){
    
    Scanner myScanner = new Scanner (System.in);
    
    double pyramidVolume;
    
    System.out.print("The square side of the pyramid is (input length): ");
    double pyramidLength = myScanner.nextDouble();
    
    System.out.print("The height of the pyramid is (input height): ");
    double pyramidHeight = myScanner.nextDouble();
    
      pyramidVolume = (((Math.pow(pyramidLength,2))*1/3)*pyramidHeight);
      System.out.println("The volume inside the pyramid is: "+pyramidVolume);
    
  }
}