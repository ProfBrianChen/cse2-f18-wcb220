//lab07, 10/24/2018, William Barish

import java.util.Random;
import java.util.Scanner;
public class Methods {


  public static String adjective (String adj){
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        adj = "big";
        break;
      case 1:
        adj = "small";
        break;
      case 2:
        adj = "tall";
        break;
      case 3:
        adj = "short";
        break;
      case 4:
        adj = "high";
        break;
      case 5:
        adj = "low";
        break;
      case 6:
        adj = "cool";
        break;
      case 7:
        adj = "boring";
        break;
      case 8:
        adj = "amazing";
        break;
      case 9:
        adj = "weird";
        break;
    }
    return adj;
    }
  public static String verb (String v){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        v = "hit";
        break;
      case 1:
        v = "ate";
        break;
      case 2:
        v = "ran";
        break;
      case 3:
        v = "flew";
        break;
      case 4:
        v = "jumped";
        break;
      case 5:
        v = "drove";
        break;
      case 6:
        v = "threw";
        break;
      case 7:
        v = "broke";
        break;
      case 8:
        v = "passed";
        break;
      case 9:
        v = "yeeted";
        break;
    }
    return v;
    }
  public static String subject (String subj){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        subj = "boy";
        break;
      case 1:
        subj = "girl";
        break;
      case 2:
        subj = "dad";
        break;
      case 3:
        subj = "mom";
        break;
      case 4:
        subj = "teacher";
        break;
      case 5:
        subj = "man";
        break;
      case 6:
        subj = "dog";
        break;
      case 7:
        subj = "woman";
        break;
      case 8:
        subj = "cat";
        break;
      case 9:
        subj = "baby";
        break;
    }
    return subj;
    }
  public static String object (String obj){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        obj = "cow";
        break;
      case 1:
        obj = "horse";
        break;
      case 2:
        obj = "fox";
        break;
      case 3:
        obj = "car";
        break;
      case 4:
        obj = "house";
        break;
      case 5:
        obj = "ball";
        break;
      case 6:
        obj = "water";
        break;
      case 7:
        obj = "food";
        break;
      case 8:
        obj = "phone";
        break;
      case 9:
        obj = "hat";
        break;
    }
    return obj;
    }
  public static void main (String [] args){
    String adj = "adj";
    String subj = "subj";
    String obj = "obj";
    String v = "v";
    int a;
    String userAnswer;
    int b;
    
    Scanner scan = new Scanner (System.in);
    
    
    
    System.out.println("The " + adjective(adj) + " " + adjective(adj) + " " + subject(subj) + " " + verb(v) + " the " + object(obj) + ".");
    System.out.println("Would you like another sentence? (Yes or No)");
      userAnswer = scan.nextLine();
    if (userAnswer == "Yes"){
      System.out.println("The " + adjective(adj) + " " + adjective(adj) + " " + subject(subj) + " " + verb(v) + " the " + object(obj) + ".");
    }
     else{
       
     }
     
  }
  }




  
