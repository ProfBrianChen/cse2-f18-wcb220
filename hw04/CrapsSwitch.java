//hw04, Craps Switch statements, William Barish
import java.util.Scanner;

public class CrapsSwitch {
  public static void main (String [] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    int userNum1 = 0;
    int userNum2 = 0;
    char userResponse;
    System.out.println("Would you like to pick the two numbers? (Y or N): ");
    userResponse = myScanner.next().charAt(0);
    if (userResponse == 'Y'){
      System.out.println("Pick a number from 1 to 6: ");
      userNum1 = myScanner.nextInt();
      System.out.println("Pick another number from 1 to 6: ");
      userNum2 = myScanner.nextInt();
  }
    else if (userResponse == 'N'){
      userNum1 = (int) (Math.random() * 6 + 1);
      userNum2 = (int) (Math.random() * 6 + 1);
      System.out.println(userNum1);
      System.out.println(userNum2);
    }
    switch (userNum1){
      case 1: 
        switch (userNum2){
          case 1:
          System.out.println("Snake Eyes");
          break;
          case 2:
          System.out.println("Ace Deuce");
          break;
          case 3:
          System.out.println("Easy Four");
          break;
          case 4:
          System.out.println("Fever Five");
          break;
          case 5:
          System.out.println("Easy Six");
          break;
          case 6:
          System.out.println("Seven Out");
          break;
        }
      break;
      case 2:
        switch (userNum2){
          case 1:
          System.out.println("Ace Deuce");
          break;
          case 2:
          System.out.println("Hard Four");
          break;
          case 3:
          System.out.println("Fever Five");
          break;
          case 4:
          System.out.println("Easy Six");
          break;
          case 5:
          System.out.println("Seven Out");
          break;
          case 6:
          System.out.println("Easy Eight");
          break;
        }
      break;
      case 3:
        switch (userNum2){
          case 1:
          System.out.println("Easy Four");
          break;
          case 2:
          System.out.println("Fever Five");
          break;
          case 3:
          System.out.println("Hard Six");
          break;
          case 4:
          System.out.println("Seven Out");
          break;
          case 5:
          System.out.println("Easy Eight");
          break;
          case 6:
          System.out.println("Nine");
          break;
        }
      break;
      case 4:
        switch (userNum2){
          case 1:
          System.out.println("Fever Five");
          break;
          case 2:
          System.out.println("Easy Six");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Hard Eight");
          break;
          case 5:
          System.out.println("Nine");
          case 6:
          System.out.println("Easy Ten");
          break;
        }
      break;
      case 5:
        switch (userNum2){
          case 1:
          System.out.println("Easy Six");
          break;
          case 2:
          System.out.println("Seven Out");
          break;
          case 3:
          System.out.println("Easy Eight");
          break;
          case 4:
          System.out.println("Nine");
          break;
          case 5:
          System.out.println("Hard Ten");
          break;
          case 6:
          System.out.println("Yo-leven");
          break;
        }
      break;
      case 6:
        switch (userNum2){
          case 1:
          System.out.println("Seven Out");
          break;
          case 2:
          System.out.println("Easy Eight");
          break;
          case 3:
          System.out.println("Nine");
          break;
          case 4:
          System.out.println("Easy Ten");
          break;
          case 5:
          System.out.println("Yo-leven");
          break;
          case 6:
          System.out.println("Box Cars");
          break;
        }
      break;
        }
    } // end of main method
} // end of class

