//lab08, 11/7/2018, William Barish
public class Arrays {
  public static void main (String [] args){
    
    int x = 0;
    int i;
    int c = 0;
    int temp = 0;
    int [] array1 = new int [100];
    int [] array2 = new int [100];
    for (i = 0; i < 100; i++){
    array1[i] = (int)(Math.random()*100-1);
    }
  
    do{
    array2[x] = x;
    x++;
    } while (x < 100);
    
    for (int a = 0; a < 100; a++){
      for (int b = 0; b < 100; b++){
        if (array2[c] == array1[b]){
          temp++;
        }
      }
      System.out.println(c+" occurs "+temp+" times.");
      c++;
      temp = 0;
    }
    
  }
}