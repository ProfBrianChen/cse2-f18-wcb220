//cse2, 11/30/18, William Barish
import java.util.Scanner;

public class tictactoe {
  
  public static boolean winner(String[][] array){
    
    
      if ((array[0][0] == "X")&&(array[1][0] == "X")&&(array[2][0] == "X")){
          System.out.println("Winner!");
          return true;
        }
      else if((array[0][0] == "X")&&(array[1][1] == "X")&&(array[2][2] == "X")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][0] == "X")&&(array[0][1] == "X")&&(array[0][2] == "X")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[1][0] == "X")&&(array[1][1] == "X")&&(array[1][2] == "X")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[2][0] == "X")&&(array[2][1] == "X")&&(array[2][2] == "X")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][1] == "X")&&(array[1][1] == "X")&&(array[2][1] == "X")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][2] == "X")&&(array[1][2] == "X")&&(array[2][2] == "X")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][2] == "X")&&(array[1][1] == "X")&&(array[2][0] == "X")){
        System.out.println("Winner!");
        return true;
      }
      
      if ((array[0][0] == "O")&&(array[1][0] == "O")&&(array[2][0] == "O")){
          System.out.println("Winner!");
          return true;
        }
      else if((array[0][0] == "O")&&(array[1][1] == "O")&&(array[2][2] == "O")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][0] == "O")&&(array[0][1] == "O")&&(array[0][2] == "O")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[1][0] == "O")&&(array[1][1] == "O")&&(array[1][2] == "O")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[2][0] == "O")&&(array[2][1] == "O")&&(array[2][2] == "O")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][1] == "O")&&(array[1][1] == "O")&&(array[2][1] == "O")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][2] == "O")&&(array[1][2] == "O")&&(array[2][2] == "O")){
        System.out.println("Winner!");
        return true;
      }
      else if((array[0][2] == "O")&&(array[1][1] == "O")&&(array[2][0] == "O")){
        System.out.println("Winner!");
        return true;
      }
      return false;
      
    }
    
  
  
  public static void main (String [] args){
    Scanner scan = new Scanner (System.in);
    int inputRow = 0;
    int inputColumn = 0;
    int count = 0;
    String[][] board = new String[3][3];
    
    board[0][0] = "1";
    board[0][1] = "2";
    board[0][2] = "3";
    board[1][0] = "4";
    board[1][1] = "5";
    board[1][2] = "6";
    board[2][0] = "7";
    board[2][1] = "8";
    board[2][2] = "9";
    
    for (int i = 0; i < board.length; i++){
      for (int j = 0; j < board[i].length; j++){
        System.out.print(board[i][j]+" ");
      }
      System.out.println();
    }
    
    
    for (int i = 0; i < 9; i++){
      
      
       System.out.println("Enter the row you want to mark (0-2)"); 
      inputRow = scan.nextInt();
      if ((inputRow > 2)){
        System.out.println("Invalid entry, try again");
        System.out.println("Enter the row you want to mark (0-2)");
        inputRow = scan.nextInt();
      }
      else if (inputRow < 0){
        System.out.println("Invalid entry, try again");
        System.out.println("Enter the row you want to mark (0-2)");
        inputRow = scan.nextInt();
      }
      
      
      
      System.out.println("Enter the column you want to mark (0-2)");
      inputColumn = scan.nextInt();
      if (inputColumn > 2){
        System.out.println("Invalid entry, try again");
        System.out.println("Enter the column you want to mark (0-2)");
        inputColumn = scan.nextInt();
      }
      else if (inputColumn < 0){
        System.out.println("Invalid entry, try again");
        System.out.println("Enter the column you want to mark (0-2)");
        inputColumn = scan.nextInt();
      }
      
      if ((board[inputRow][inputColumn] == "X") || (board[inputRow][inputColumn] == "O")){
        System.out.println("This position has already been taken, try again");
        System.out.println("Enter the row you want to mark (0-2)");
      if (!scan.hasNextInt()){
        System.out.println("Invalid entry, try again");
      }
      else{
        inputRow = scan.nextInt();
      }
      System.out.println("Enter the column you want to mark (0-2)");
      if (!scan.hasNextInt()){
        System.out.println("Invalid entry, try again");
      }
      else{
        inputColumn = scan.nextInt();
      }
      }
      System.out.println();
      if ((inputRow >= 0) && (inputRow <= 2)){
        if ((inputColumn >= 0) && (inputColumn <= 2)){
          if (count%2 == 0){
            board[inputRow][inputColumn] = "X";
          }
          else {
            board[inputRow][inputColumn] = "O";
          }
          count += 1;
          for (int a = 0; a < board.length; a++){
            for (int b = 0; b < board[a].length; b++){
              System.out.print(board[a][b]+" ");
              
            }
            System.out.println();
          }
          if (winner(board) == true){
            break;
          }
        }
        
      }
      if (i == 8){
        System.out.println("Draw!");
        break;
      }
    }

    
  }
}