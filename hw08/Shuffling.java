// 11/15/2018, hw08, William Barish
import java.util.Scanner;
public class Shuffling{
  
  public static String printArray(String[] list){
    for (int a = 0; a < 52; a++){
      System.out.print(list[a]+" ");
    }
    System.out.println();
  }
  public static String shuffle (String[] list){
    for (int a = 0; a < 52; a++){
      int temp = (int)(Math.random()*52-1);
      System.out.println(list[temp]);
    }
  }
  public static String[] getHand(String list[], int index, int numCards){
    for (int a = 0; a < 4; a++){
      int temp = (int)(Math.random()*52-1);
      System.out.println(list[temp]);
    }
  }
  
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
}
