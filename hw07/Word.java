// hw07, 11/6/2018, William Barish

import java.util.Scanner;

public class Word {
    
  public static int getNumOfNonWSCharacters(String a){
  a = a.replace(" ", "");
  int length = a.length( );
  return length;
}
  public static int getNumOfWords(String a){
    int i = 1;
    for (int x = 1; x < a.length(); x++){
      if (a.charAt(x) == ' '){
        if (a.charAt(x+1) != ' '){
          i++;
        }
      }
    }
    return i;
  }
  public static int findText (String a, String b){
        
    int wordFind = 0;
    for (int i = 0; i < 5; i++){
      if (a.indexOf(b, i) >= 1){
      wordFind++;
      }
    }
    System.out.println(b + " occurences: "+wordFind);
    return wordFind;
  }
  public static String replaceExclamation (String a){
    a = a.replace("!",".");
    return a;
  }
  public static String shortenString (String a){
    do{
      a = a.replace("  ", " ");
    } while (a == "  ");
    return a;
  }
  
  public static void main (String [] args){
    Scanner scan = new Scanner (System.in);
    String junk;
    
    System.out.println("Enter a sample text:");
    String userString = scan.nextLine();
    System.out.println("You entered:");
    System.out.println(userString);
    
    System.out.println("MENU");
    System.out.println("c - Number of non-white characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option:");
    
    String menu;
   
     do{ 
       menu = scan.nextLine();
      if (menu.charAt(0) == 'c'){
        System.out.println("Number of non-white characters: "+getNumOfNonWSCharacters(userString));
      }
      else if (menu.charAt(0) == 'w'){
        System.out.println("Number of words: " + getNumOfWords(userString));
      }
      else if (menu.charAt(0) == 'f'){
        System.out.println("Enter a word or phrase to be found");
        String word = scan.nextLine();
        System.out.print(findText(userString, word));
      }
      else if (menu.charAt(0) == 'r'){
        System.out.println("Edited Text: "+replaceExclamation(userString));
      }
      else if (menu.charAt(0) == 's'){
        System.out.println("Edited Text: "+shortenString(userString));
      }
      else if (menu.charAt(0) == 'q'){
        break;
      }
      else {
        System.out.println("Please enter a valid character.");
      }
       
       
     } while (menu.charAt(0) != 'q');
    
    
    
    
    
  }
}