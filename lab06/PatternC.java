//Lab 06, Pattern C, 10/17/2018, William Barish

import java.util.Scanner;

public class PatternC {
  public static void main (String [] args) {
    
    Scanner scan = new Scanner (System.in);
    
    int a; //variable for first for statement
    int b; //variable for second statement
    int numRows; //this is the number of rows the user asks for
    String junk; 
    
    System.out.println("Enter an integer between 1 and 10: "); //asks the user for an integer between 1 and 10
    
    while ((!scan.hasNextInt())){ //if it isnt an int, invalid entry pops up and user types in another value
      System.out.println("Invalid entry");
      junk = scan.nextLine();
    }
    numRows = scan.nextInt(); //once through the while statement, numrows gets its value
    junk = scan.nextLine();
    
    while ((numRows > 10) || (numRows < 1)){ //if number is outside range, user has to put in a value in correct range
      System.out.println("Invalid entry");
      junk = scan.nextLine();
      System.out.println("Enter an integer between 1 and 10: "); //asks the user for an integer between 1 and 10
      while ((!scan.hasNextInt())){
      System.out.println("Invalid entry");
      junk = scan.nextLine();
    }
      numRows = scan.nextInt();
      junk = scan.nextLine();
    }
    
    for (a = 1; a <= numRows; a++){
      for (b = ((numRows-a)*2); b >= a; b--){
        System.out.print(" ");
        continue;
      }
      for (b = 1; b <= a; b++){
        System.out.print(b+" ");
        continue;
      }
      System.out.println();
    }
  }
}

    
    
    
    
    