////hw02, William Barish

public class Arithmetic{
  public static void main (String[] args) {

//Number of pairs of pants
int numPants=3;
//Cost per pair of pants
double pantsPrice=34.98;

//Number of sweatshirts
int numShirts=2;
//cost per shirt
double shirtPrice=24.99;

//Number of belts
int numBelts=3;
//cost per belt
double beltCost=33.99;

//the tax rate
double paSalesTax=0.06;
    
//Total cost of each kind of item
double totalCostOfPants=numPants*pantsPrice; //total cost of pants
double totalCostOfShirts=numShirts*shirtPrice; //total cost of shirts
double totalCostOfBelts=numBelts*beltCost; //total cost of belts

    System.out.println ("Total cost of pants is $"+totalCostOfPants);
    System.out.println ("Total cost of shirts is $"+totalCostOfShirts);
    System.out.println ("Total cost of belts is $"+totalCostOfBelts);
    
//Sales tax charged buying all of each kind of item
double totalSalesTaxPants=(numPants*pantsPrice)*paSalesTax; //tax for pants
    double finalSalesTaxPants=Math.round(totalSalesTaxPants*100)/100; //tax for pants rounded
double totalSalesTaxShirts=(numShirts*shirtPrice)*paSalesTax; // tax for shirts
    double finalSalesTaxShirts=Math.round(totalSalesTaxShirts*100)/100; //tax for shirts rounded
double totalSalesTaxBelts=(numBelts*beltCost)*paSalesTax; // tax for belts
    double finalSalesTaxBelts=Math.round(totalSalesTaxBelts*100)/100; //tax for belts rounded

    System.out.println ("Sales tax for pants is $"+finalSalesTaxPants);
    System.out.println ("Sales tax for shirts is $"+finalSalesTaxShirts);
    System.out.println ("Sales tax for belts is $"+finalSalesTaxBelts);
    
//Total cost of purchases without tax
double totalCostAllPurchases=(numPants*pantsPrice)+(numShirts*shirtPrice)+
(numBelts*beltCost);
    double finalCostAllPurchases=(totalCostAllPurchases*100)/100.0; //cost of all purchases without tax rounded
    
    System.out.println ("The total cost before tax is $"+finalCostAllPurchases);
  
//Total sales tax
double totalSalesTax=totalCostAllPurchases*paSalesTax; //Total sales tax for all items
    double finalSalesTax=Math.round(totalSalesTax*100)/(100.0); //total sales tax for all items rounded

    System.out.println ("The total tax in this transaction is $"+finalSalesTax);
    
//Total paid for this transaction, including sales tax
double totalCost=totalCostAllPurchases+totalSalesTax; //Total for transaction
    double finalTotalCost=Math.round(totalCost*100)/(100.0); //Final cost for the transaction with tax, rounded
  
    System.out.println ("The total cost of this transaction is $"+finalTotalCost);
    
} //end of main method
} //end of hw02
